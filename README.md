<h1>Introduction</h1>
The goal of dimensionality reduction is to map the high dimensional samples to a lower dimensional space which might not be a subset of the original feature set but still preserving certain properties from the original set.

Principal component analysis is one of the most popular dimensionality reduction algorithm that's used in data mining. Limitation of PCA is that it tries to find a linear projection which maximizes the preserved variance in the data. This method is often susceptible to noise and often ignores the global structure that might exists within the dataset. 


In this project, two dimensionality reduction algorithm that resolves the issues in PCA were implemented.

**Locality Preserving Projection:**
Unlike PCA, LPP are linear projection maps that arise by solving a variational problem that optimally preserves the neighbourhood structure of the data set.  In other words, LPP looks at the local structure instead of the global structure. The LPP algorithm builds a graph that incorporates neighborhood information of the data set. Using the notion of the Laplacian of the graph, the linear transformation matrix that maps the data point to a subspace is computed.

**Neighborhood Preserving Embedding:**
NPE is another linear DR method that aims at preserving the local manifold structure through graph-based embedding manifold. Although LPP and NPE have similar goals, the objective functions of both methods are completely different. NPE models the manifold structure directly by constructing the k-nearest-neighbor(KNN) graph that reveals neighborhood relations of data points, and then retains the graph structure in the projection.