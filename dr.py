import os
import numpy as np
from numpy.dual import eigh
from scipy import linalg
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.manifold._locally_linear import barycenter_kneighbors_graph
from sklearn.metrics import euclidean_distances, pairwise_distances
from sklearn.utils import check_array
from sklearn.neighbors import NearestNeighbors, kneighbors_graph
from sklearn import datasets
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from statistics import mean
from scipy.linalg import blas as bl
from scipy.sparse import linalg, eye
import time
from sklearn import preprocessing


def rbf(dist, t=1.0):
    '''
    rbf kernel function
    '''
    return np.exp(-(dist / t))


def cal_pairwise_dist(x):
    sum_x = np.sum(np.square(x), 1)
    dist = np.add(np.add(-2 * np.dot(x, x.T), sum_x).T, sum_x)

    return dist


def cal_rbf_dist(data, n_neighbors=10, t=1):
    dist = cal_pairwise_dist(data)
    dist[dist < 0] = 0
    n = dist.shape[0]
    rbf_dist = rbf(dist, t)

    W = np.zeros((n, n))
    for i in range(n):
        index_ = np.argsort(dist[i])[1:1 + n_neighbors]
        W[i, index_] = rbf_dist[i, index_]
        W[index_, i] = rbf_dist[index_, i]

    return W


# class implementation for neighbourhood preserving embedding
class NPE:
    def __init__(self, neighbors=5, t=1):
        self.type = "NPE"
        self.neighbors = neighbors
        self.t = t

    def fit(self, data):
        data = check_array(data)

        nbrs = NearestNeighbors(n_neighbors=self.neighbors + 1, n_jobs=-1)
        nbrs.fit(data)

        W = barycenter_kneighbors_graph(
            nbrs, n_neighbors=self.neighbors, n_jobs=-1
        )

        adjacency_weight = W

        I = np.identity(adjacency_weight.shape[0])
        A = (I - adjacency_weight)
        M = np.dot(A.T, A)

        N = data.shape[0]
        W = cal_rbf_dist(data, self.neighbors, self.t)
        D = np.zeros_like(W)
        for i in range(N):
            D[i, i] = np.sum(W[i])

        XXT = np.dot(np.dot(data.T, D), data)
        XMXT = np.dot(np.dot(data.T, M), data)

        self.eig_val, self.eig_vec = np.linalg.eig(np.dot(np.linalg.pinv(XXT), XMXT))
        self.sort_index_ = np.argsort(np.abs(self.eig_val))

        return self

    def transform(self, data, components):
        print("running transform")
        return self.extract_component(data, components)

    def extract_component(self, data, components):
        eig_val = self.eig_val[self.sort_index_]

        j = 0
        while eig_val[j] < 1e-6:
            j += 1

        sort_index_ = self.sort_index_[j:j + components]

        eig_vec_picked = self.eig_vec[:, sort_index_]

        data_ndim = np.dot(data, eig_vec_picked)

        return data_ndim


class NewLpp:
    def __init__(self, n_neighbors, t=1):
        self.n_neighbors = n_neighbors
        self.t = t

    def fit(self, data):
        N = data.shape[0]
        W = cal_rbf_dist(data, self.n_neighbors, self.t)
        D = np.zeros_like(W)

        for i in range(N):
            D[i, i] = np.sum(W[i])

        L = D - W
        XDXT = np.dot(np.dot(data.T, D), data)
        XLXT = np.dot(np.dot(data.T, L), data)

        self.eig_val, self.eig_vec = np.linalg.eig(np.dot(np.linalg.pinv(XDXT), XLXT))
        self.sort_index_ = np.argsort(np.abs(self.eig_val))

        return self

    def transform(self, data, components):
        return self.extract_component(data, components)

    def extract_component(self, data, components):
        eig_val = self.eig_val[self.sort_index_]

        j = 0
        while eig_val[j] < 1e-6:
            j += 1

        sort_index_ = self.sort_index_[j:j + components]

        eig_vec_picked = self.eig_vec[:, sort_index_]

        data_ndim = np.dot(data, eig_vec_picked)

        return data_ndim


n_neighbor = 20
n_components = 5
if __name__ == "__main__":
    data = datasets.load_breast_cancer()
    x1 = data.data

    dist = cal_pairwise_dist(x1)
    max_dist = np.max(dist)

    npe = NPE(n_neighbor, max_dist)
    npe = npe.fit(x1)
    npe_reduced = npe.transform(x1, n_components)

    lpp = NewLpp(n_neighbor, max_dist)
    lpp = lpp.fit(x1)
    lpp_reduced = lpp.transform(x1, n_components)
